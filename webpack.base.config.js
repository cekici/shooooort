const path = require('path');
const srcPath = path.resolve(__dirname, "src");
const distPath = path.resolve(__dirname, 'dist');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const postCssConfig = require("./postcss.config.js");

module.exports = {
  entry: srcPath + "/index.jsx",
  output: {
    filename: 'bundle.js',
    path: distPath
  },

  devtool: 'inline-source-map',
  devServer: {
    contentBase: distPath,
    hot: true
  },

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          cacheDirectory: true
        },
      },
      {
        test: /\.scss/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                importLoaders: 1
              }
            },
            {
              loader: "postcss-loader",
              options: postCssConfig
            },
            {
              loader: "sass-loader"
            }
          ]
        })
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        exclude: /node_modules/,
        loader: "url-loader",
        options: {
          name: "fonts/[name].[ext]"
        }
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new ExtractTextPlugin({
      filename: "[name].css",
      disable: false,
      allChunks: true
    }),
    new HtmlWebpackPlugin({
      template: `${srcPath}/index.html`,
      filename: "index.html"
    })
  ]
};