const path = require('path');
const webpack = require("webpack");
const distPath = path.resolve(__dirname, 'dist');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpackMerge = require("webpack-merge");
const webpackBaseConfig = require('./webpack.base.config.js');


module.exports = webpackMerge(webpackBaseConfig, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: distPath,
    hot: true
  },
  plugins: [
    new CleanWebpackPlugin([distPath]),
    new webpack.HotModuleReplacementPlugin()
  ]
});