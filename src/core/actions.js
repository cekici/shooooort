const FULFILLED_KEYWORD = 'FULFILLED';
const REJECTED_KEYWORD = 'REJECTED';

function createActions(actionName) {
  return {
    base: actionName,
    get fulfilled() {
      return `${this.base}_${FULFILLED_KEYWORD}`;
    },
    get rejected() {
      return `${this.base}_${REJECTED_KEYWORD}`;
    },
  };
}

export const SHORTEN_URL = createActions('SHORTEN_URL');
export const CLEAR_URL_LIST = createActions('CLEAR_URL_LIST');
export const GET_URL_STATS = createActions('GET_URL_STATS');
