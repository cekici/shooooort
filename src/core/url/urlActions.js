import * as ACTIONS from '../../core/actions';

export const shortenUrl = params => ({
  type: ACTIONS.SHORTEN_URL.base,
  payload: {
    params,
  },
});

export const shortenUrlFulfilled = response => ({
  type: ACTIONS.SHORTEN_URL.fulfilled,
  payload: response,
});

export const shortenUrlRejected = error => ({
  type: ACTIONS.SHORTEN_URL.rejected,
  payload: error,
  isError: true,
});

export const clearUrlList = () => ({
  type: ACTIONS.CLEAR_URL_LIST.base,
});

export const getUrlStats = shortcode => ({
  type: ACTIONS.GET_URL_STATS.base,
  payload: {
    shortcode,
  },
});

export const getUrlStatsFulfilled = (shortcode, response) => ({
  type: ACTIONS.GET_URL_STATS.fulfilled,
  payload: {
    shortcode,
    response,
  },
});

export const getUrlStatsRejected = error => ({
  type: ACTIONS.GET_URL_STATS.rejected,
  payload: error,
  isError: true,
});
