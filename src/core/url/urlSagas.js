import { call, put, takeEvery } from 'redux-saga/effects';

import * as ACTIONS from '../../core/actions';
import {
  getUrlStatsFulfilled,
  getUrlStatsRejected,
  shortenUrlFulfilled,
  shortenUrlRejected,
} from './urlActions';
import urlApi from './urlApi';

function* shortenUrl(action) {
  try {
    const response = yield call(urlApi.shortenUrl, action.payload.params);
    yield put(shortenUrlFulfilled(response));
  } catch (e) {
    yield put(shortenUrlRejected(e));
  }
}

function* getUrlStats(action) {
  try {
    const response = yield call(urlApi.getUrlStats, action.payload.shortcode);
    yield put(getUrlStatsFulfilled(action.payload.shortcode, response));
  } catch (e) {
    yield put(getUrlStatsRejected(e));
  }
}

const userSagas = [
  takeEvery(ACTIONS.SHORTEN_URL.base, shortenUrl),
  takeEvery(ACTIONS.GET_URL_STATS.base, getUrlStats),
];

export default userSagas;

