import * as ACTIONS from '../../core/actions';

const defaultInitialState = {
  urlList: [],
  urlApiError: {},
  hasActiveShortenUrlAction: false,
  candidateShortenUrl: {},
};

function UrlReducer(initialState = defaultInitialState) {
  return (state = initialState, action) => {
    let newState;

    switch (action.type) {
      case ACTIONS.SHORTEN_URL.base: {
        newState = {
          ...state,
          hasActiveShortenUrlAction: true,
          candidateShortenUrl: action.payload.params.url,
        };
        break;
      }

      case ACTIONS.SHORTEN_URL.fulfilled: {
        const newUrl = {
          url: state.candidateShortenUrl,
          shortcode: action.payload.data.shortcode,
        };

        const urlList = [
          newUrl,
          ...state.urlList,
        ];

        newState = {
          ...state,
          urlList,
          urlApiError: {},
          hasActiveShortenUrlAction: false,
          candidateShortenUrl: null,
        };

        break;
      }

      case ACTIONS.SHORTEN_URL.rejected: {
        newState = {
          ...state,
          urlApiError: {
            type: 'api',
            name: 'shortcode',
            message: action.payload.response.data,
          },
          hasActiveShortenUrlAction: false,
          candidateShortenUrl: null,
        };

        break;
      }

      case ACTIONS.CLEAR_URL_LIST.base: {
        newState = {
          ...state,
          urlList: [],
        };

        break;
      }


      case ACTIONS.GET_URL_STATS.fulfilled: {
        const { shortcode, response } = action.payload;
        const { urlList } = state;
        const {
          lastSeenDate,
          redirectCount,
        } = response.data;

        const urlIndex = urlList.findIndex(urlItem => urlItem.shortcode === shortcode);
        let nextUrlList = [
          ...urlList,
        ];

        if (urlIndex > -1) {
          const url = nextUrlList[urlIndex];

          nextUrlList = [
            ...nextUrlList.slice(0, urlIndex),
            {
              ...url,
              lastSeenDate,
              redirectCount,
            },
            ...nextUrlList.slice(urlIndex + 1),
          ];
        }

        newState = {
          ...state,
          urlList: nextUrlList,
          urlApiError: {},
          hasActiveShortenUrlAction: false,
          candidateShortenUrl: null,
        };

        break;
      }

      default:
        newState = state;
        break;
    }

    return newState;
  };
}

export default UrlReducer;
