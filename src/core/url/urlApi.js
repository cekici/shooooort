import networkManager from '../../common/network/networkManager';

const urlApi = {
  shortenUrl(params) {
    return networkManager.api.post('/shorten', params);
  },

  getUrlStats(shortcode) {
    return networkManager.api.get(`/${shortcode}/stats`);
  },
};

export default urlApi;
