import axios from 'axios';

const baseURL = 'http://localhost:3000/';

const networkManager = {
  api: axios.create({
    baseURL,
  }),
};

export default networkManager;
