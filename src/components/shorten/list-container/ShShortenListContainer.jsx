import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ShShortenListItem from '../list-item/ShShortenListItem';

import './_sh-shorten-list-container.scss';

export default class ShShortenList extends Component {
  static propTypes = {
    urlList: PropTypes.array.isRequired,
  }

  render() {
    const {
      urlList,
    } = this.props;

    return (
      <div className="sh-shorten-list-container">
        <div className="sh-shorten-list-container__header">
          <span className="sh-shorten-list-container__header-link">LINK</span>
          <span className="sh-shorten-list-container__header-visit-count">VISITS</span>
          <span className="sh-shorten-list-container__header-last-visit">LAST VISITED</span>
        </div>

        <ul className="sh-shorten-list">
          {urlList.map(urlItem =>
            (<ShShortenListItem
              urlItem={urlItem}
              key={urlItem.shortcode}
            />
            ))}
        </ul>
      </div>
    );
  }
}
