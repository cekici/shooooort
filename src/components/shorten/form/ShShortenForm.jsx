import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ShButton from '../../ui/button/ShButton';
import ShInput from '../../ui/input/ShInput';

import './_sh-shorten-form.scss';

export default class ShShortenForm extends Component {
  static propTypes = {
    apiError: PropTypes.object.isRequired,
    hasActiveShortenUrlAction: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      url: '',
      shortcode: '',
      formError: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.apiError !== this.props.apiError) {
      this.setState({
        formError: {
          ...nextProps.apiError,
        },
      });
    }

    if (!nextProps.hasActiveShortenUrlAction && this.props.hasActiveShortenUrlAction &&
      !nextProps.apiError.type) {
      this.setState({
        url: '',
        shortcode: '',
        formError: {},
      });
    }
  }

  handleQueryChange = (event) => {
    this.setState({
      formError: {},
      url: event.target.value,
    });
  }

  handleShortCodeChange = (event) => {
    this.setState({
      formError: {},
      shortcode: event.target.value,
    });
  }


  handleSubmit = (event) => {
    const { onSubmit } = this.props;
    const { url, shortcode } = this.state;

    event.preventDefault();

    if (!url) {
      this.setState({
        formError: {
          type: 'field',
          name: 'url',
          message: "URL can't be empty",
        },
      });
    } else {
      onSubmit(url, shortcode);
    }
  }

  render() {
    const { hasActiveShortenUrlAction } = this.props;
    const { formError, url, shortcode } = this.state;

    const hasError = !!formError.message;

    return (
      <form
        className="sh-shorten-form"
        onSubmit={this.handleSubmit}
      >

        <section className="sh-shorten-form__field-list-section">
          <ShInput
            type="input"
            value={url}
            name="url"
            className="sh-shorten-form__input"
            placeholder="Paste the link you want to shorten here"
            hasError={formError.name === 'url'}
            onChange={this.handleQueryChange}
          />

          <ShInput
            type="input"
            value={shortcode}
            name="shortcode"
            className="sh-shorten-form__input"
            placeholder="Preferential Shortcode"
            hasError={formError.name === 'shortcode'}
            onChange={this.handleShortCodeChange}
          />

          {
            hasError &&
            <p className="sh-shorten-form__error">
              {formError.message}
            </p>
          }
        </section>

        <ShButton
          type="submit"
          className="sh-shorten-form__submit-button"
          isDisabled={hasActiveShortenUrlAction || !url}
          isLoading={hasActiveShortenUrlAction}
          text="Shorten this link"
        />
      </form>);
  }
}
