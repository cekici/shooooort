import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import classNames from 'classnames';
import CopyToClipboard from 'react-copy-to-clipboard';

import './_sh-shorten-list-item.scss';

const BASE_URL = 'http://impraise-shorty.herokuapp.com/';

export default class ShShortenListItem extends Component {
  static propTypes = {
    urlItem: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props);

    this.state = {
      isClipboardInfoTextVisible: false,
    };
  }

  setIsClipboardInfoTextVisible = () => {
    this.setState({
      isClipboardInfoTextVisible: true,
    });
  }

  unsetIsClipboardInfoTextVisible = () => {
    this.setState({
      isClipboardInfoTextVisible: false,
    });
  }

  render() {
    const { urlItem } = this.props;
    const { isClipboardInfoTextVisible } = this.state;

    const {
      url,
      shortcode,
      lastSeenDate,
      redirectCount,
    } = urlItem;

    const shortLinkUrl = `${BASE_URL}${shortcode}`;

    let formattedLastVisit = '';

    if (lastSeenDate) {
      const today = moment();
      const lastVisit = moment(lastSeenDate);
      formattedLastVisit = today.to(lastVisit);
    } else {
      formattedLastVisit = 'Never';
    }

    const copyClipboardInfoTextClassName = classNames('sh-shorten-list-item__short-link-clipboard-info-text', {
      'is-visible': isClipboardInfoTextVisible,
    });

    return (
      <li className="sh-shorten-list-item">
        <div className="sh-shorten-list-item__url-info-container">
          <div className="sh-shorten-list-item__short-link-container">
            <CopyToClipboard text={shortLinkUrl}>
              <p
                className="sh-shorten-list-item__short-link"
                onMouseOver={this.setIsClipboardInfoTextVisible}
                onMouseOut={this.unsetIsClipboardInfoTextVisible}
                onFocus={this.setIsClipboardInfoTextVisible}
                onBlur={this.unsetIsClipboardInfoTextVisible}
              >
                <span className="sh-shorten-list-item__short-link-base">shooooort.com/</span>
                <span className="sh-shorten-list-item__short-link-shortcode">{shortcode}</span>
              </p>
            </CopyToClipboard>

            <span className={copyClipboardInfoTextClassName}>
              {'Click to copy this link'}
            </span>
          </div>

          <a
            className="sh-shorten-list-item__url"
            href={url}
            target="_blank"
          >
            {url}
          </a>
        </div>
        <span className="sh-shorten-list-item__visit-count">{`${redirectCount}`}</span>
        <span className="sh-shorten-list-item__last-visit">{formattedLastVisit}</span>

      </li>
    );
  }
}
