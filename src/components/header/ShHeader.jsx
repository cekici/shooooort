import React, { Component } from 'react';

import './_sh-header.scss';

export default class ShHeader extends Component {
  render() {
    return (
      <header className="sh-header">
        <h1 className="sh-header__title">Shooooort</h1>
        <span className="sh-header__motto">The link shortener with a long name</span>
      </header>
    );
  }
}
