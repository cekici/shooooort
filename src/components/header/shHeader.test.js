import React from 'react';
import renderer from 'react-test-renderer';

import ShHeader from './ShHeader.jsx';

test('it renders correctly', () => {
  const component = renderer.create(<ShHeader/>);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});