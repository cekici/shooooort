import React from 'react';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';

import ShInput from './ShInput.jsx';

const mockOnChangeFunc = jest.fn();
const inputProps = {
  type: "text",
  value: "",
  name: "input",
  placeholder: "input",
  onChange: mockOnChangeFunc
}

test('it renders correctly', () => {
  const component = renderer.create(
    <ShInput {...inputProps}/>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('has is-error class when hasError prop is passed as true', () => {
  const wrapper = shallow(
    <ShInput {...inputProps}
             hasError={true}/>
  );

  const input = wrapper.find('.sh-input');
  expect(input.hasClass("has-error")).toEqual(true);
});

test('passed onChange prop to be called on input change', () => {
  const inputVal = "input val";

  const wrapper = shallow(
    <ShInput {...inputProps}/>
  );

  const input = wrapper.find('.sh-input');
  input.simulate('change', { target: { value: inputVal } });

  expect(mockOnChangeFunc).toBeCalled();
});