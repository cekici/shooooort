import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './_sh-input.scss';

export default class ShInput extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    className: PropTypes.string,
    hasError: PropTypes.bool,
  }

  static defaultProps = {
    className: '',
    hasError: false,
  }

  render() {
    const {
      type,
      value,
      name,
      placeholder,
      onChange,
      className,
      hasError,
    } = this.props;

    const inputClassName = classNames('sh-input', className, {
      'has-error': hasError,
    });

    return (
      <input
        type={type}
        value={value}
        name={name}
        placeholder={placeholder}
        onChange={onChange}
        className={inputClassName}
      />
    );
  }
}
