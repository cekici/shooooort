import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import ShButton from './ShButton.jsx';

test('it renders correctly', () => {
  const component = renderer.create(
    <ShButton type="button"/>
  );

  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('has is-loading class when isLoading prop is passed as true', () => {
  const wrapper = shallow(
    <ShButton type="button"
              isLoading={true}/>
  );

  const button = wrapper.find('.sh-button');
  expect(button.hasClass("is-loading")).toEqual(true);
});

test('passed onClick prop to be called on button click', () => {
  const mockFunc = jest.fn();

  const wrapper = shallow(
    <ShButton type="button"
              onClick={mockFunc}/>
  );

  const button = wrapper.find('.sh-button');
  button.simulate('click');

  expect(mockFunc).toBeCalled();
});