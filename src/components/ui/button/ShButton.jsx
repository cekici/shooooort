import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import './_sh-button.scss';

export default class ShButton extends Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
    text: PropTypes.string,
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    onClick: PropTypes.func.isRequired,
  }

  static defaultProps = {
    text: '',
    className: '',
    isDisabled: false,
    isLoading: false,
  }

  handleClick = (event) => {
    const { onClick } = this.props;

    if (onClick) {
      onClick(event);
    }
  }

  render() {
    const {
      className, text, isDisabled, isLoading, type,
    } = this.props;

    const buttonClassName = classNames('sh-button', className, {
      'is-loading': isLoading,
    });

    return (
      <button
        className={buttonClassName}
        disabled={isDisabled}
        type={type}
        onClick={this.handleClick}
      >
        <div className="sh-button__content">
          {isLoading ?
            <svg
              className="sh-button__loader"
              width="20"
              height="20"
              viewBox="0 0 52 52"
              xmlns="http://www.w3.org/2000/svg"
            >

              <circle
                className="sh-button__loader-path"
                cx="26"
                cy="26"
                r="20"
                fill="none"
                strokeWidth="6"
              />
            </svg> :
            <span className="sh-button__text">
              {text}
            </span>
          }
        </div>
      </button>);
  }
}
