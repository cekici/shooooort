import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ShShortenForm from '../../components/shorten/form/ShShortenForm';
import ShShortenListContainer from '../../components/shorten/list-container/ShShortenListContainer';
import * as urlActions from '../../core/url/urlActions';

import './_sh-dashboard-page.scss';

class ShDashboardPage extends Component {
  static propTypes = {
    urlList: PropTypes.array.isRequired,
    urlApiError: PropTypes.object.isRequired,
    hasActiveShortenUrlAction: PropTypes.bool.isRequired,
  }

  componentWillMount() {
    const { dispatch, urlList } = this.props;

    urlList.forEach((urlItem) => {
      dispatch(urlActions.getUrlStats(urlItem.shortcode));
    });
  }

  componentWillReceiveProps(nextProps) {
    const { dispatch } = this.props;

    if (!nextProps.hasActiveShortenUrlAction && this.props.hasActiveShortenUrlAction &&
      !nextProps.urlApiError.type) {
      dispatch(urlActions.getUrlStats(nextProps.urlList[0].shortcode));
    }
  }

  handleFormSubmit = (url, shortcode) => {
    const { dispatch } = this.props;

    const params = {
      url,
    };

    if (shortcode) {
      params.shortcode = shortcode;
    }

    dispatch(urlActions.shortenUrl(params));
  }

  handleClearHistoryLinkClick = (e) => {
    const { dispatch } = this.props;

    e.preventDefault();

    dispatch(urlActions.clearUrlList());
  }

  render() {
    const {
      urlList,
      urlApiError,
      hasActiveShortenUrlAction,
    } = this.props;

    return (
      <div className="sh-dashboard-page">
        <ShShortenForm
          apiError={urlApiError}
          hasActiveShortenUrlAction={hasActiveShortenUrlAction}
          onSubmit={this.handleFormSubmit}
        />

        <div className="sh-dashboard-page__url-list-header">
          <h2 className="sh-dashboard-page__url-list-title">
            {'Previously shortened by you'}
          </h2>
          <button
            type="button"
            className="sh-dashboard-page__url-list-clear-link"
            onClick={this.handleClearHistoryLinkClick}
          >
            Clear History
          </button>
        </div>

        <ShShortenListContainer urlList={urlList} />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    urlState: {
      urlList,
      urlApiError,
      hasActiveShortenUrlAction,
    },
  } = state;

  return {
    urlList,
    urlApiError,
    hasActiveShortenUrlAction,
  };
}

const ConnectedShDashboardPage = connect(mapStateToProps)(ShDashboardPage);
const ConnectedShDashboardPageWithRouter = withRouter(ConnectedShDashboardPage);

export default ConnectedShDashboardPageWithRouter;
