/* eslint-disable */
/* react/no-multi-comp*/

import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import ROUTE_PATHS from '../core/routes';
import ShHeader from '../components/header/ShHeader';
import ShDashboardPage from '../pages/dashboard/ShDashboardPage';

import './_sh-app.scss';

class ShSmartApp extends Component {
  render() {
    return (
      <div className="sh-app">
        <ShHeader />

        <Switch>
          <Route
            path={ROUTE_PATHS.ROUTE_DASHBOARD}
            component={ShDashboardPage}
          />

          <Redirect
            from="/*"
            to={ROUTE_PATHS.ROUTE_DASHBOARD}
          />
        </Switch>
      </div>
    );
  }
}

class ShRootSmartApp extends Component {
  render() {
    return (
      <Router basename="/">
        <Route component={ShSmartApp} />
      </Router>
    );
  }
}

/* eslint-enable */

export default ShRootSmartApp;
