import { createStore, combineReducers, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';

import urlSagas from '../core/url/urlSagas';
import UrlReducer from '../core/url/UrlReducer';
import { loadState, saveState } from '../common/utils/persistanceUtils';

const persistedState = loadState();

function* rootSaga() {
  yield all([
    ...urlSagas,
  ]);
}

const rootReducer = combineReducers({
  urlState: new UrlReducer(),
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  persistedState,
  applyMiddleware(sagaMiddleware),
);

sagaMiddleware.run(rootSaga);


store.subscribe(() => {
  saveState(store.getState());
});

export default store;

