URL Shortener App
===========================
A single-page web application for URL shortener

## App

To install:
```
npm install
```

To build:
```
npm run build
```

To run development:
```
npm run dev
```

To test:
```
npm run test
```

To lint:
```
npm run eslint
```


## Development Server
A tiny dev server is added for eliminating CORS.

To build:
```
cd server
npm install
```

To run: 
```
node server.js
```

