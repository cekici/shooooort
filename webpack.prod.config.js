const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpackMerge = require("webpack-merge");
const webpackBaseConfig = require('./webpack.base.config.js');

module.exports = webpackMerge(webpackBaseConfig, {
  plugins: [
    new UglifyJSPlugin()
  ]
});